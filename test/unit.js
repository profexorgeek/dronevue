/**
 * Mocha Unit Tests
 */



 /**
  * Dependencies
  */
var assert = require('assert');
var DroneStore = require('../app/dronestore');
var DroneModel = require('../app/models/drone');


var testDroneData = {
    id: 'abcdef',
    latitude: -1,
    longitude: -1,
    altitude: -1,
    speed: -1
};

 /**
  * Drone Model Tests
  */
 describe('DroneModel', function () {
    var drone = null;

    describe('#constructor', function () {
        it('should return an instance of DroneModel', function () {
            drone = new DroneModel();
            assert(drone instanceof DroneModel);
        });
    });

    describe('#updateFromData()', function () {
        it('should set drone properties based on data passed.', function () {
            drone.updateFromData(testDroneData);
            assert.strictEqual(drone.id, testDroneData.id);
            assert.strictEqual(drone.latitude, testDroneData.latitude);
            assert.strictEqual(drone.longitude, testDroneData.longitude);
            assert.strictEqual(drone.altitude, testDroneData.altitude);
            assert.strictEqual(drone.speed, testDroneData.speed);
        });
    });

    describe('#refreshActiveStatus()', function () {
        it('should set active property to false', function () {
            // force some properties then make sure active status is true
            drone.active = true;
            drone.lastUpdate = 0;
            assert.strictEqual(drone.active, true);

            drone.refreshActiveStatus();
            assert.strictEqual(drone.active, false);
        });
    });
});

/**
 * Drone Store Tests
 */
describe('DroneStore', function () {
    describe('#update()', function () {
        it('should return a DroneModel instance.', function () {
            var drone = DroneStore.update(testDroneData);
            assert(drone instanceof DroneModel);
        });

        it('should throw a TypeError', function () {
            assert.throws(function () {
                var drone = DroneStore.update();
            }, TypeError);
        })
    });

    describe('#getAll()', function () {
        it('should return a single drone added by the update test.', function () {
            var drones = DroneStore.getAll();
            assert.strictEqual(drones.length, 1);
            assert.strictEqual(drones[0].id, testDroneData.id);
        });
    });

    describe('#remove()', function () {
        it('should return a single drone added by the update test.', function () {
            var drone = DroneStore.remove(testDroneData.id);
            assert(drone);
            assert(drone instanceof DroneModel);
            assert.strictEqual(drone.id, testDroneData.id);
        });
    });

    describe('#getAll()', function () {
        it('should return zero drones.', function () {
            var drones = DroneStore.getAll();
            assert.strictEqual(drones.length, 0);
        });
    });
});
var assert = require('assert');
var axios = require('axios');
var apiUrl = 'http://localhost:8080/api/'
var droneUrl = apiUrl + 'drones/';
var testDroneData = {
    id: 'abcdef',
    latitude: -1,
    longitude: -1,
    altitude: -1,
    speed: -1
};

// TODO: Let's see if you're paying attention. These tests
// always appear to succeed, even if the server isn't
// running. I'm not familiar with how to perform
// integration testing with Mocha. This was a
// failed experiment. Discuss.
describe('Back End API', function () {
    describe("#/api/ (GET)", function  () {
        it('should verify the server is online', function () {
            axios.get(apiUrl, {})
                .then(function (response) {
                    assert(response);
                    assert.strictEqual(response.online, true);
                })
                .catch(function () {
                    //assert.fail('Server down? Failed to GET: ' + apiUrl);
                });
        })
    });

    describe('#/api/drones/ (POST)', function () {
        it('should add a drone instance', function () {
            axios.post(droneUrl, testDroneData)
            .then(function (response) {
                assert(response);
                assert.strictEqual(response.success, true);
            })
            .catch(function () {
                //assert.fail('Failed to POST drone to: ' + droneUrl);
            })
        });
    });

    describe('#api/drones/ (GET)', function () {
        it('should get the drone previous test added', function () {
            axios.get(droneUrl, {})
            .then(function (response) {
                assert(response);
                assert(Array.isArray(response));
                assert.strictEqual(response.length, 1);
                
                var drone = response[0];
                assert.strictEqual(drone.id, testDroneData.id);
                assert.strictEqual(drone.latitude, testDroneData.latitude);
                assert.strictEqual(drone.longitude, testDroneData.longitude);
                assert.strictEqual(drone.altitude, testDroneData.altitude);
                assert.strictEqual(drone.speed, testDroneData.speed);
            })
            .catch(function () {
                //assert.fail('Failed to GET drones from: ' + droneUrl);
            });
        });
    });

    describe('#api/drones/ (DELETE)', function () {
        it('should remove the drone previous test added', function () {
            axios.delete(droneUrl, testDroneData)
            .then(function (response) {
                assert(response);
                var drone = response;
                assert.strictEqual(drone.id, testDroneData.id);
                assert.strictEqual(drone.latitude, testDroneData.latitude);
                assert.strictEqual(drone.longitude, testDroneData.longitude);
                assert.strictEqual(drone.altitude, testDroneData.altitude);
                assert.strictEqual(drone.speed, testDroneData.speed);
            })
            .catch(function () {
                //assert.fail('Failed to POST drone to: ' + droneUrl);
            });
        });
    });

    describe('#api/drones/ (GET)', function () {
        it('should get an empty set', function () {
            axios.get(droneUrl, {})
            .then(function (response) {
                assert(response);
                assert(Array.isArray(response));
                assert.strictEqual(response.length, 0);
            })
            .catch(function () {
                //assert.fail('Failed to GET drones from: ' + droneUrl);
            })
        })
    })
});
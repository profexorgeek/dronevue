# DroneVue Application

## Requirements
A company has a number of drones flying around the country. You have been tasked to build a system to track the location of every drone in real-time. The system's dashboard will only display the last location of the drones, so the backend doesn't need to worry about the history. You can store the state of the application in-memory for simplicity reasons.

Each drone should be associated with a unique identifier, and should report its geo-location coordinates to the central server in real-time through a cellular modem connection. Cellular modem connections are expensive, therefore you need to make sure the drones report back their location using as little data as possible.

The dashboard should be a simple single-page application displaying the list of active drones, by their unique identifiers, along with their current speed. You should visually highlight the drones that have not been moving for more than 10 seconds.

## Assumptions
* Drones are only highlighted if they haven't moved for 10 seconds so "realtime" can have significant latency.
* Maintaining a persistent connection via cellular is fragile and requires both battery and data. Thus, drone-to-server updates should be fire-and-forget
* Requirements state that dashboard should highlight drones that haven't moved. However, the server does not store history. Also tracking stationary drones may be desired. Thus, "haven't moved" likely means "haven't reported in"
* Since the drone network is not yet live, the system should be able to simulate a drone network for testing.
* Dashboard requirements imply that dashboard primary purpose is to show general location and speed. The dashboard refresh rate is too low to issue commands. Drones are assumed to have their own, separate navigation system.
* Requirements state that dashboard will "display the last location" but also states that it should display "active drones, by their unique identifiers, along with their current speed". It's assumed that both the location and the speed are desired in the dashboard.

## Proposed Solution
If this system was likely to issue navigation commands to drones or perform other low-latency tasks some type of socket connection system or messaging queue might be appropriate. However, given the requirements a simple REST API is best-fit. It's highly-reliable, allows fire-and-forget updates, is simple to build and operates on a wide range of hardware.

Using a RESTful API for the drone network also allows the same server to easily host the dashboard too.

In simulations, update request size was consistently under 250 bytes. This means a drone posting updates every 5 seconds will consume less than 200k bandwidth per hour of flight time. This estimate is _without_ gzipping requests and includes more data points than strictly specified so this could be further optimized. It's also worth noting that this estimate does not include any overhead for an authentication system.

## Implementation
Given only high-level requirements, this implementation strives for simplicity above all else and focuses on an MVP. It minimizes dependencies and uses plain JS where possible. It should be easy to train, easy to deploy and easy to extend.

### Back End
The back end utilizes Node.js, requiring only Express and Body Parser.

The Back End defines these endpoints:

* **/ (GET)** - Dashboard
* **/api/ (GET)** - Simple way to check if the server is up
* **/api/drones/ (POST)** - Accepts a data object with id, latitude, longitude, altitude and speed properties. New IDs will be added, existing IDs will be updated so all drones can mindlessly POST to the same URL.
* **/api/drones/ (DELETE)** - Removes a drone
* **/api/drones/ (GET)** - Gets an array of drones


Primary components and their duties:

* **server.js** - Starts the server
* **app/router.js** - defines API routes
* **app/dronestore.js** - Pure JS in-memory data store
* **app/models/drone.js** - Drone model capable of populating itself from a generic data object

_NOTE: the server currently cleans up (deletes) drones that haven't reported in for a period of time. This is mostly nice for testing and may not be desirable in production_

### Front End
The front end utilizes the most bare-bones Vue implementation possible. It loads bootstrap, axios, fontawesome and vue from popular CDNs, leveraging browser caching for many users. It requires zero building. Primary components and their duties:

* **index.html** - Dashboard HTML file, loads dependencies from CDNs
* **app.js** - Single JS file defining Vue app and components
* **simulate.js** - Pure JS drone network simulator (requires Axios for ajax requests)

## Dashboard User Instructions
Assuming the server is running, visit [Dashboard] (http://localhost:8080/). Click "Start Simulating" to fake a drone network. This will spawn 10 drones that report random locations and speeds. A percentage of the drones will fail to send updates, which will be shown on the dashboard with a warning color and icon. The dashboard updates every 5 seconds.

## Technical Instructions
### Tooling Recommendations

* VSCode with Docker and Vetur extensions
* Postman for API testing
* Chrome debugging tools
* Terminal (Mac) or Git Bash (Win) for CLI, Testing
* Node 10.X

### Directly Running
* Run `npm install` to install dependencies
* Run `npm test` to run Mocha tests
* Run `npm start` to run server
* Visit `localhost:8080` for dashboard

### Docker

* Build the docker file `docker build -t dronevue .`
* This should fetch npm and docker modules, unit test the project and build a docker image
* Check that the "dronevue" image exists `docker image ls`
* Run the docker image on port 8080 `docker run -p 8080:8080 dronevue`
* If you want to run the image in detached mode use `docker run -d -p 8080:8080 dronevue`
* You should see the container running: `docker container ls`

_NOTE: This application expects to run on port 8080. Other applications you may be running such as MAMP or other Docker images may interfere with usage of this port. This will require changes to either your environment or this app configuration beyond the scope of these instructions!_

## Dev Notes
My philosophy is to always strive for the simplest solution with the least dependencies.

Prior to building this I had no experience with Express, Docker, Mocha or Axios. I had no experience with Node.js as a back end stack in general. I had minimal experience with Vue. I chose these tools because they were best-fit for the target company environment and to demonstrate my ability to work outside of my comfort zone. I'm sure I broke a lot of rules. Educate me :)

/**
 * Application Entry Point
 * 
 * Configures and starts server.
 */

'use strict';

var config = {
   port: process.env.PORT || 8080
};

/**
* Dependencies
*/
var express = require('express');
var router = require('./app/router');
var bodyParser = require('body-parser');

/**
* Configure server
*/
var server = express();
server.use(bodyParser.urlencoded({ extended: true}));
server.use(bodyParser.json());
server.use('/', express.static('public'));
server.use('/api', router);

/**
* Start
*/
server.listen(config.port);
console.log('Drone server is running on ' + config.port);
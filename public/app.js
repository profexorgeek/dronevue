var app = new Vue({
    el: '#app',
    data: {
        drones: [],
    },
    methods: {
        toggleSimulator: function (evt) {
            simulator.toggle();
        },
        simulating: function () {
            return simulator.isRunning();
        },
        simulationStyle: function () {
            return simulator.isRunning() ? 'btn-danger' : 'btn-success';
        },
        simulationButtonText: function () {
            return simulator.isRunning() ? 'Stop Simulating' : 'Start Simulating';
        }
    }
});

Vue.component('drone-item', {
    props: ['drone'],
    // TODO: this is hacky but more readable without requiring a full
    // build process for .vue files!
    template:   '<li class="list-group-item" :class="itemStyle"><div class="row">' +
                '<div class="col-sm-1"><i class="fa" v-bind:class="getIcon"></i></div>' +
                '<div class="col-sm-3">{{drone.id}}</div>' +
                '<div class="col-sm-4">({{drone.latitude}},{{drone.longitude}})</div>' +
                '<div class="col-sm-4">{{drone.speed}}kph</div>' +
                '</div></li>',
    computed: {
        itemStyle : function () {
            return this.drone.active ? 'list-group-item-success' : 'list-group-item-warning';
        },
        getIcon: function () {
            return this.drone.active ? 'fa-signal' : 'fa-exclamation-circle';
        }
    }
});


/**
 * Simple timer to poll API
 * 
 * TODO: replace this with a real service
 * that integrates more nicely with Vue!
 * 
 * TODO: Magic numbers!? Don't hardcode
 * the interval.
 */
var timer = window.setInterval(updateData, 5000);
function updateData() {
    axios.get('http://localhost:8080/api/drones/', {})
    .then(function (response) {
        console.log('Got ' + response.data.length + ' drones.');
        app.$data.drones = response.data;
    })
    .catch(function (error) {
        console.log(error);
    });
}

updateData();
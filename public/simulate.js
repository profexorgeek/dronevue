/**
 * Simulator
 * 
 * Simulates a drone network. Requires
 */
var simulator = (function (ax) {
    // TODO: most of this belongs in a config
    var drones = [],
        tickFreq = 5000,
        timer = null,
        numDrones = 10,
        maxLat = 90,
        maxLng = 180,
        maxAlt = 10000,
        maxSpeed = 100,
        chanceToFailUpdate = 0.4,
        simulating = false;

    function getDroneName() {
        return Math.random().toString(36).replace(/[^a-z0-9]+/g, '').substr(0, 10);
    }

    function getRandLat() {
        return (Math.random() * (maxLat * 2)) - maxLat;
    }

    function getRandLng() {
        return (Math.random() * (maxLng * 2)) - maxLng;
    }

    function getRandAlt() {
        return (Math.random() * maxAlt);
    }

    function getRandSpeed() {
        return (Math.random() * maxSpeed);
    }

    function createDrone() {
        var drone = {
            id: getDroneName()
        }

        // randomize properties
        updateDrone(drone);

        // TODO: check if id is actually unique
        drones.push(drone);
    }

    function round(num) {
        return Math.round(num * 100) / 100;
    }

    function updateDrone(drone) {
        drone.latitude = round(getRandLat());
        drone.longitude = round(getRandLng());
        drone.altitude = round(getRandAlt());
        drone.speed = round(getRandSpeed());
    }

    function start() {
        simulating = true;
        while(drones.length < numDrones) {
            createDrone();
        }

        if(timer == null) {
            timer = window.setInterval(tick, tickFreq);
        }

        tick();

        console.log("Drone simulation started.");
    }

    function stop() {
        simulating = false;
        drones.forEach(function (drone) {
            destroyDrone(drone);
        });

        drones = [];

        console.log("Drone simulation stopped");
    }

    function tick() {
        if(simulating) {
            console.log("Simulation sending updates for " + drones.length + " drones.");
            drones.forEach(function (drone) {
                if(Math.random() > chanceToFailUpdate) {
                    updateDrone(drone);
                    sendDrone(drone);
                }
            });
        }
    }

    function destroyDrone(drone) {
        axios.delete('http://localhost:8080/api/drones/', drone)
            .then(function(response) {
                // nothing needed here
            })
            .catch(function (error) {
                console.log('Error posting drone data:' + error);
            });
    }

    function sendDrone(drone) {
        axios.post('http://localhost:8080/api/drones/', drone)
            .then(function(response) {
                // nothing needed here
            })
            .catch(function (error) {
                console.log('Error posting drone data:' + error);
            });
    }

    return {
        start: start,
        stop: stop,
        tick: tick,
        toggle: function () { simulating ? stop() : start(); },
        isRunning: function () { return simulating; }
    };

})(axios);
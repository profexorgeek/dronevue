/**
 * DroneModel
 * 
 * Simple JS model with a helper method that allows
 * the model to update itself from a request body.
 */

'use strict';

var secondsBeforeInactive = 10;


function DroneModel () {
    this.id = '';
    this.latitude = 0;
    this.longitude = 0;
    this.altitude = 0;
    this.lastUpdate = 0;
    this.speed = 0;
    this.active = false;
}

DroneModel.prototype.updateFromData = function (droneData, errorCallback) {
    var errors = [];

    // TODO: this should do a sanity check on droneData here and either
    // throw an exception or add errors to the array and hit the callback

    this.id = droneData.id;
    this.latitude = droneData.latitude;
    this.longitude = droneData.longitude;
    this.altitude = droneData.altitude;
    this.speed = droneData.speed;
    this.lastUpdate = Date.now();
    this.active = true;

    // TODO: this will never be called because errors array isn't implemented!
    if(errors.length > 0 && errorCallback && typeof(errorCallback) === "function") {
        errorCallback.call(this, errors);
    }
};

DroneModel.prototype.refreshActiveStatus = function() {
    this.active = false;
    if(Date.now() - this.lastUpdate < secondsBeforeInactive * 1000) {
        this.active = true;
    }
}

module.exports = DroneModel;
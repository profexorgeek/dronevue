/**
 * Router
 * 
 * Simple router for the only endpoints in this API.
 */

'use strict';

/**
 * Dependencies
 */
var express = require('express');
var store = require('./dronestore');

/**
 * Routes
 */
var router = express.Router();

router.get('/', function(req, res) {
    res.json({
        online: true,
        message: 'Drone server online.'
    });
});

router.route('/drones')
    // fire-and-forget handles new or existing drones
    .post(function(req, res) {
        var success = true;
        store.update(req.body);

        // TODO: make sure this actually did succeed and
        // maybe return the object?
        res.json({success: true});
    })

    .delete(function(req, res) {
        var id = req.params.id;
        var drone = store.remove(id);
        res.json(drone);
    })

    // retrieves the current list of drones
    .get(function(req, res) {
        res.json(store.getAll());
    });

module.exports = router;
/**
 * DroneStore
 * 
 * Extremely simple in-memory storage of drones.
 * Could be expanded into a repository pattern or
 * other persistance system.
 */

 /**
  * Dependencies
  */
 var DroneModel = require('./models/drone');

 /**
  * Store
  */
 var drones = [],
     inactiveSecondsToDestroy = 30,
     droneStore = {
 
      update: function (droneData) {
          var drone = drones.find(function(d) {
              if(d.id === droneData.id) {
                  return true;
              }
          });
 
          if(drone === undefined) {
             console.log('New drone reporting in.');
              drone = new DroneModel();
              drones.push(drone);
          }
 
          drone.updateFromData(droneData);
          console.log('Updated drone with id: ' + drone.id);
 
          return drone;
      },
 
      remove: function(id) {
          var drone = drones.find(function (d) {
              if(d.id === id) {
                  return true;
              }
          }),
          i = drones.indexOf(drone);
          drones.splice(i, 1);
          return drone;
      },
 
      refreshActiveStatus: function () {
         for(var i = drones.length - 1; i > -1; i--) {
             drones[i].refreshActiveStatus();
             if(Date.now() - drones[i].lastUpdate > (inactiveSecondsToDestroy * 1000)) {
                 console.log("Drone inactive too long, assuming lost: " + drones[i].id);
                 drones.splice(i, 1);
             }
         }
      },
 
      getAll: function () {
          this.refreshActiveStatus();
          return drones;
      }
  }
 
  module.exports = droneStore;